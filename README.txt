
INTRODUCTION:
------------
This module is used to generate reward points for every user upon login at 
specified intervals.

INSTALLATION:
-------------
1. Place the entire user login points folder into your Drupal modules/
   directory.

2. Enable the User Login Points module by navigating to:

     extend > modules

CONFIGURATION:
--------------
1. Once the module is installed, you will find a User Login Point link under
   Configuration->System
   here you can configure the user points per login and the time interval.
   Make sure to check the checkbox to enable the user point logic for all
   users. 

2. After configuration, every time when the user logs in a message will be
   displayed showing the current user login points.

Author:
-------
Jack Ry
