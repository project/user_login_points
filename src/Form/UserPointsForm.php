<?php

namespace Drupal\user_login_points\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UserPointsForm.
 */
class UserPointsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'user_login_points.userpoints',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_points_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_login_points.userpoints');
    $group = 'user_login_points';
    $form[$group] = [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('User Login Points'),
      '#group' => 'settings_additional',
      '#weight' => 40,
    ];
    $form[$group]['user_login_points'] = [
      '#type' => 'textfield',
      '#title' => t('Points per user log in'),
      '#default_value' => $config->get('user_login_points'),
      '#size' => 5,
      '#maxlength' => 5,
      '#required' => TRUE,
    ];

    $form[$group]['user_login_points_interval'] = [
      '#type' => 'select',
      '#title' => t('Minimum interval between logins to accrue points'),
      '#default_value' => $config->get('user_login_points_interval'),
      '#options' => ['120' => '120', '3600' => '3600', '10800' => '10800', '21600' => '21600', '32400' => '32400', '43200' => '43200', '86400' => '86400', '172800' => '172800', '259200' => '259200', '604800' => '604800', '1209600' => '1209600', '241920' => '2419200'],
      '#required' => TRUE,
    ];

    $form[$group]['user_login_points_enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Check this to enable user points for all users'),
      '#default_value' => $config->get('user_login_points_enable'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('user_login_points.userpoints')
      ->set('user_login_points', $form_state->getValue('user_login_points'))
      ->set('user_login_points_interval', $form_state->getValue('user_login_points_interval'))
      ->set('user_login_points_enable', $form_state->getValue('user_login_points_enable'))
      ->save();
  }

}
